### VIDYO plugin

- **Description:** super simple script that targets all videos (of a certain selector) and adds custom controls to them rather than using the current browser's defaults.
- **Author:** HT (@&hairsp;glenthemes)

:warning:&ensp;still in beta, expect bugs and crashes. Also does not work on mobile (touch devices uses their native player).

An updated and slightly more stable version can be found at:  
[github.com/ht-devx/VIDYO](https://github.com/ht-devx/VIDYO)

#### Preview:
🐻&ensp;[vidyo-demo.glitch.me](https://vidyo-demo.glitch.me)

#### Demo code:
🐻&ensp;[jsfiddle.net/glenthemes/hd0o8c2z](https://jsfiddle.net/glenthemes/hd0o8c2z)

#### How to use:
Paste this under `<head>` or `<body>` of your HTML:
```html
<!--✻✻✻✻✻✻  VIDYO (video plugin) by @glenthemes  ✻✻✻✻✻✻-->
<link href="https://vid_yo.gitlab.io/o/core.css" rel="stylesheet">
<script src="https://vid_yo.gitlab.io/o/init.js"></script>
<script>
	VIDYO("video"); // replace 'video' with your video(s) selector
</script>
```

Styling has already been included in `core.css`, but you can customize it as you like. Here is a full list of the available appearance options (CSS variables):
```css
:root, :host {
	--VIDYO-buttons-color:#fff;
	--VIDYO-buttons-size:2rem;
	--VIDYO-buttons-shadow-strength:10%;
	--VIDYO-hover-speed:0.25s;
	
	--VIDYO-slider-area-height:40px;
	--VIDYO-slider-area-background:#000;
	--VIDYO-slider-area-transparency:75%;
	--VIDYO-slider-area-blur:0px;
	--VIDYO-slider-area-side-padding:12px;
	
	--VIDYO-slider-bar-width:69%;
	--VIDYO-slider-bar-height:5px;
	--VIDYO-slider-bar-color:#232323;
	--VIDYO-slider-bar-roundness:5px;
	--VIDYO-slider-bar-fill-color:#eee;
	--VIDYO-slider-gaps:15px;
	
	--VIDYO-knob-width:12px;
	--VIDYO-knob-height:12px;
	--VIDYO-knob-color:#fafafa;
	--VIDYO-knob-roundness:100%;
	
	--VIDYO-tiny-buttons-size:0.8rem;
	--VIDYO-tiny-buttons-color:#efefef;
	
	--VIDYO-time-color:#efefef;
	--VIDYO-time-font-family:"instrument sans";
	--VIDYO-time-font-size:0.8rem;
	
	--VIDYO-volume-slider-bar-width:5px;
	--VIDYO-volume-slider-bar-height:80px; /* % not allowed */
	--VIDYO-volume-slider-bar-color:#232323;
	--VIDYO-volume-slider-bar-roundness:5px;
	--VIDYO-volume-slider-bar-fill-color:#8e8e8e;
	--VIDYO-volume-slider-touch-padding:15px;
	--VIDYO-volume-slider-fade-speed:0.3s;
	--VIDYO-volume-slider-padding:12px;
	--VIDYO-volume-knob-width:12px;
	--VIDYO-volume-knob-height:12px;
	--VIDYO-volume-knob-roundness:100%;
	--VIDYO-volume-knob-color:#efefef;
}
```

#### Issues & Troubleshooting:
VIDYO is still in beta and unreleased to the public. If you happen to have found this and have questions or found any issues, please let me know!  
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

#### Found this tool helpful?
Please consider sending me a donation, thank you!  
&boxh;&ensp;[ko-fi.com/glenthemes](https://ko-fi.com/glenthemes) :coffee:
